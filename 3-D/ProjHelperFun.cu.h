#ifndef PROJ_HELPER_FUNS
#define PROJ_HELPER_FUNS

#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include "Constants.h"

using namespace std;

struct RollArrs {
    REAL *u; 
    REAL *v; 
    REAL *a; 
    REAL *b; 
    REAL *c; 
    REAL *y; 
    REAL *yy;
    REAL *tmp_u;
    REAL *tmp_v;
    REAL *tmp_myResult;
    REAL *tmp_myVar; // small
    RollArrs(   const unsigned int& outer,
                const unsigned int& numX,
                const unsigned int& numY) {
        const unsigned int size = outer * numX * numY;

        cudaMalloc((void**)&(this->u), size * sizeof(REAL));
        cudaMalloc((void**)&(this->v), size * sizeof(REAL));
        cudaMalloc((void**)&(this->a), size * sizeof(REAL));
        cudaMalloc((void**)&(this->b), size * sizeof(REAL));
        cudaMalloc((void**)&(this->c), size * sizeof(REAL));
        cudaMalloc((void**)&(this->y), size * sizeof(REAL));
        cudaMalloc((void**)&(this->yy), size * sizeof(REAL));
        cudaMalloc((void**)&(this->tmp_u), size * sizeof(REAL));
        cudaMalloc((void**)&(this->tmp_v), size * sizeof(REAL));
        cudaMalloc((void**)&(this->tmp_myResult), size * sizeof(REAL));
        cudaMalloc((void**)&(this->tmp_myVar), numX * numY * sizeof(REAL));
    }

    void free() {
        cudaFree(this->u);
        cudaFree(this->v);
        cudaFree(this->a);
        cudaFree(this->b);
        cudaFree(this->c);
        cudaFree(this->y);
        cudaFree(this->yy);
        cudaFree(this->tmp_u);
        cudaFree(this->tmp_v);
        cudaFree(this->tmp_myResult);
        cudaFree(this->tmp_myVar);
    }


}  __attribute__ ((aligned (128)));

struct PrivGlobs {

    //	grid
    REAL *              myX;        // [numX]
    REAL *              myY;        // [numY]
    REAL *              myTimeline; // [numT]
    REAL *              d_myTimeline; // [numT]
    unsigned            myXindex;  // 
    unsigned            myYindex; // 

    //	variable
    REAL * myResult;

    //	coeffs
    REAL * myVarX; // [numX][numY], but we alwasy transpose
    REAL * myVarY; // [numX][numY]
    

    //	operators
    REAL *   myDxx;  // [numX][4]
    REAL *   myDyy;  // [numY][4]

    PrivGlobs( ) {
        printf("Invalid Contructor: need to provide the array sizes! EXITING...!\n");
        exit(0);
    }

    // Allocates memory for use in cuda
    PrivGlobs(  const bool cuda,
                const unsigned int& outer,
                const unsigned int& numX,
                const unsigned int& numY,
                const unsigned int& numT ) {

        
        this->myTimeline = (REAL *)calloc(numT, sizeof(REAL)); // doesn't need to be on kernel
        cudaMalloc((void**)&(this->d_myTimeline), numT * sizeof(REAL));

        cudaMalloc((void**)&(this->myX), numX * sizeof(REAL));
        cudaMalloc((void**)&(this->myY), numY * sizeof(REAL));
        cudaMalloc((void**)&(this->myDxx), numX * 4 * sizeof(REAL));
        cudaMalloc((void**)&(this->myDyy), numY * 4 * sizeof(REAL));

        cudaMalloc((void**)&(this->myVarX), numX * numY * sizeof(REAL));
        cudaMalloc((void**)&(this->myVarY), numX * numY * sizeof(REAL));

        cudaMalloc((void**)&(this->myResult), outer * numX * numY * sizeof(REAL));

    }

    void free(bool cuda) {
        cudaFree(this->d_myTimeline);
        cudaFree(this->myX);
        cudaFree(this->myY);
        cudaFree(this->myDxx);
        cudaFree(this->myDyy);
        cudaFree(this->myVarX);
        cudaFree(this->myVarY);
        cudaFree(this->myResult);
    }

} __attribute__ ((aligned (128)));

void updateParams(const unsigned it, 
                  const unsigned int&   numX,
                  const unsigned int&   numY, 
                  const REAL alpha, 
                  const REAL beta, 
                  const REAL nu, 
                  PrivGlobs& cudas);

void setPayoff(const unsigned int&   outer, 
               const unsigned int&   numX,
               const unsigned int&   numY,
               PrivGlobs& cudas );

void tridag(
    REAL *  a,   // size [n]
    REAL *  b,   // size [n]
    REAL *  c,   // size [n]
    REAL *  r,   // size [n]
    const int             n,
          REAL *          u,   // size [n]
          REAL *          uu   // size [n] temporary
);

void rollback(const unsigned        it, 
              const unsigned int&   outer, 
              const unsigned int&   numX,
              const unsigned int&   numY,
              PrivGlobs&            cudas,
              RollArrs&             arrs )
;

void run_OrigCPU(  
                const unsigned int&   outer,
                const unsigned int&   numX,
                const unsigned int&   numY,
                const unsigned int&   numT,
                const REAL&           s0,
                const REAL&           t, 
                const REAL&           alpha, 
                const REAL&           nu, 
                const REAL&           beta,
                      REAL*           res   // [outer] RESULT
            );

#endif // PROJ_HELPER_FUNS
