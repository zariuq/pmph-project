#include "Kernels.cu.h"
#include "ProjHelperFun.cu.h"
#include "Constants.h"
#include "TridagPar.cu.h"

/**
 * Fills in:
 *   globs.myTimeline  of size [0..numT-1]
 *   globs.myX         of size [0..numX-1]
 *   globs.myY         of size [0..numY-1]
 * and also sets
 *   globs.myXindex and globs.myYindex (both scalars)
 */
void initGrid(  const REAL s0, const REAL alpha, const REAL nu, const REAL t,
                const unsigned numX, const unsigned numY, const unsigned numT, 
                PrivGlobs& cudas
) {
    int dimt = ceil( (float)numT / T1);
    int dimx = ceil( (float)numX / T1 );
    int dimy = ceil( (float)numY / T1 );
    dim3 gridt(dimt, 1, 1);
    dim3 gridx(dimx, 1, 1);
    dim3 gridy(dimy, 1, 1);
    dim3 block(T1, 1, 1);

    initMyTimeLine<<<gridt, block>>>(cudas.d_myTimeline, t, numT);


    const REAL stdX = 20.0*alpha*s0*sqrt(t);
    const REAL dx = stdX/numX;
    cudas.myXindex = static_cast<unsigned>(s0/dx) % numX; // it's just a constant
    const REAL myXPlus = s0 - cudas.myXindex * dx; // To have fewer parameters to send to GPU

    initMyXY<<<gridx, block>>>(cudas.myX, dx, myXPlus);

    const REAL stdY = 10.0*nu*sqrt(t);
    const REAL dy = stdY/numY;
    const REAL logAlpha = log(alpha);
    cudas.myYindex = static_cast<unsigned>(numY/2.0);
    const REAL myYPlus = log(alpha) - cudas.myYindex * dy;

    initMyXY<<<gridy, block>>>(cudas.myY, dy, myYPlus);
    
    cudaThreadSynchronize();
    cudaMemcpy(cudas.myTimeline, cudas.d_myTimeline, numT * sizeof(REAL), cudaMemcpyDeviceToHost);

}

void initOperators(const unsigned numX, const unsigned numY,
                   PrivGlobs& cudas
) {
    int dimx = ceil( (float)numX / T1 );
    int dimy = ceil( (float)numY / T1 );
    dim3 gridx(dimx, 1, 1);
    dim3 gridy(dimy, 1, 1);
    dim3 block(T1, 1, 1);

    initOperatorKernel<<<gridx, block>>>(numX, cudas.myX, cudas.myDxx);
    initOperatorKernel<<<gridy, block>>>(numY, cudas.myY, cudas.myDyy);
    cudaThreadSynchronize();
    }

void setPayoff(const unsigned int&   outer,
               const unsigned int&   numX,
               const unsigned int&   numY, 
               PrivGlobs& cudas) {
    int dim = ceil( (float)(outer * numX * numY)/ T1 );
    dim3 grid(dim, 1, 1);
    dim3 block(T1, 1, 1);
    setPayoffKernel<<<grid, block>>>(cudas.myResult, cudas.myX, outer, numX, numY);
    cudaThreadSynchronize();
}


void updateParams(const unsigned it, 
                  const unsigned int&   numX,
                  const unsigned int&   numY,
                  const REAL alpha, 
                  const REAL beta, 
                  const REAL nu, 
                  PrivGlobs& cudas)
{
    int dim = ceil( (float)(numX * numY)/ T1 );
    dim3 grid(dim, 1, 1);
    dim3 block(T1, 1, 1);
    REAL nunuTime = 0.5 * nu * nu * cudas.myTimeline[it];
    updateParamsKernel<<<grid, block>>>(cudas.myVarX, cudas.myVarY, cudas.myX, cudas.myY, nunuTime,
                                        it, numX, numY, alpha, beta);
    cudaThreadSynchronize();
}

void sgmCPUTranspose(REAL * A, REAL * Atr, const unsigned int outer, const unsigned int rowsA, const unsigned int colsA) {
    for (int io = 0; io < outer; ++io) {
        for (int row = 0; row < rowsA; ++row) {
            for (int col = 0; col < colsA; ++col) {
                Atr[I3(io,col,row,outer,colsA,rowsA)] = A[I3(io,row,col,outer,rowsA,colsA)];
            }
        }
    }
}

void
rollback(const unsigned        it, 
         const unsigned int&   outer, 
         const unsigned int&   numX,
         const unsigned int&   numY,
         PrivGlobs&            cudas,
         RollArrs&             arrs ) {

    REAL dtInv = 1.0/(cudas.myTimeline[it+1]-cudas.myTimeline[it]);

    // u : [o][y][x]
    // v : [o][x][y]
    // a,b,c,y,yy - [o][x][y] and [o][y][x]

    const unsigned int size = outer * numX * numY;

    // For 3-D kernel flattened
    int dimoxy = ceil( (float)size / T1);
    dim3 gridoxy(dimoxy, 1, 1);  

    // For rollback kernels
    int dimox = ceil( (float)(outer * numX)/ T1 );
    int dimoy = ceil( (float)(outer * numY)/ T1 );
    dim3 gridox(dimox, 1, 1);
    dim3 gridoy(dimoy, 1, 1);
    dim3 block(T1, 1, 1);

    // For transpose
    int dimo = ceil( (float)outer / 1 );
    int dimx = ceil( (float)numX / T2 );
    int dimy = ceil( (float)numY / T2 );
    dim3 gridoytx(dimx, dimy, dimo); // for 3-D
    dim3 gridoxty(dimy, dimx, dimo);
    dim3 gridytx(dimx, dimy, 1); // for 2-D
    dim3 gridxty(dimy, dimx, 1);
    dim3 block2(T2, T2, 1);


    sgmMatTranspose<<<gridxty, block2>>>(cudas.myVarX, arrs.stmp, numX, numY); // myVarX -> myVarX'
    sgmMatTranspose<<<gridoxty, block2>>>(cudas.myResult, arrs.tmp, numX, numY); // myResult -> myResult'
    cudaThreadSynchronize();

    //cudaMemcpy(arrs.h_tmp, cudas.myResult, size * sizeof(REAL), cudaMemcpyDeviceToHost);
    //sgmCPUTranspose(arrs.h_tmp, arrs.h_tmp2, outer, numX, numY);
    //cudaMemcpy(arrs.tmp, arrs.h_tmp2, size * sizeof(REAL), cudaMemcpyHostToDevice); 

    rollBackExplicitX<<<gridox, block>>>(arrs.tmp, arrs.stmp, cudas.myDxx, arrs.u, 
                                         dtInv, outer, numX, numY);
    cudaThreadSynchronize();

    sgmMatTranspose<<<gridoytx, block2>>>(arrs.u, arrs.tmp, numY, numX); // u -> u' (replacing myResult')
    cudaThreadSynchronize();

    rollBackExplicitY<<<gridoy, block>>>(cudas.myResult, cudas.myVarY, cudas.myDyy, arrs.tmp, arrs.v, 
                                         dtInv, outer, numX, numY);
    cudaThreadSynchronize();

    sgmMatTranspose<<<gridoxty, block2>>>(arrs.tmp, arrs.u, numX, numY); // u' -> u
    rollBackImplicitX<<<gridox, block>>>(arrs.stmp, cudas.myDxx, arrs.a, arrs.b, arrs.c,
                                         dtInv, outer, numX, numY);
    cudaThreadSynchronize();
    triCallKernel<<<gridoy, block>>>(arrs.a, arrs.b, arrs.c, arrs.u, arrs.u, arrs.yy,
                                     dtInv, outer, numX, numY);
    cudaThreadSynchronize();

    sgmMatTranspose<<<gridoytx, block2>>>(arrs.u, arrs.tmp, numY, numX); // u -> u'
    cudaThreadSynchronize();

    rollBackImplicitY<<<gridoy, block>>>(cudas.myVarY, cudas.myDyy, arrs.a, arrs.b, arrs.c, 
                                         arrs.tmp, arrs.v, arrs.y, 
                                         dtInv, outer, numX, numY);
    cudaThreadSynchronize();
    triCallKernel<<<gridox, block>>>(arrs.a, arrs.b, arrs.c, arrs.y, cudas.myResult, arrs.yy,
                                     dtInv, outer, numY, numX);
    cudaThreadSynchronize();

}

void setResult(REAL* res, const unsigned int outer, const unsigned int numX, const unsigned int numY,
               PrivGlobs& cudas) {
    int dimo = ceil( (float)outer / T1 );
    dim3 grid(dimo, 1, 1);
    dim3 block(T1, 1, 1);

    REAL * d_res;
    cudaMalloc((void**)&d_res, outer * sizeof(REAL));
    setResultKernel<<<grid, block>>>(d_res, cudas.myResult, cudas.myXindex, cudas.myYindex, outer, numX, numY);
    cudaThreadSynchronize();
    cudaMemcpy(res, d_res, outer * sizeof(REAL), cudaMemcpyDeviceToHost);
    cudaFree(d_res);
}

void   run_OrigCPU(  
                const unsigned int&   outer,
                const unsigned int&   numX,
                const unsigned int&   numY,
                const unsigned int&   numT,
                const REAL&           s0,
                const REAL&           t, 
                const REAL&           alpha, 
                const REAL&           nu, 
                const REAL&           beta,
                      REAL*           res   // [outer] RESULT
) {

    PrivGlobs    cudas(true, outer, numX, numY, numT); // Cuda allocated
    RollArrs     arrs(outer, numX, numY); // Cuda allocated arrays for rollback
    initGrid(s0, alpha, nu, t, numX, numY, numT, cudas);
    initOperators(numX, numY, cudas);
    setPayoff(outer, numX, numY, cudas);

    for(int it = numT-2; it>=0; --it) {
        // Should be a straightforward kernel, passing in the data
        updateParams(it, numX, numY, alpha, beta, nu, cudas);
        // Hard part
        rollback(it, outer, numX, numY, cudas, arrs);
    }
    // Safe because each [io] array is modified independently
    setResult(res, outer, numX, numY, cudas);

    cudas.free(true);
    arrs.free();

}

//#endif // PROJ_CORE_ORIG
