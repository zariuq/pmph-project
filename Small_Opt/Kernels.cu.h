#ifndef KERS
#define KERS

#include <cuda_runtime.h>
#include "Constants.h"

// Init Kernels

__global__ void initMyTimeLine(REAL * myTimeLine, const REAL t, const unsigned numT) {
    const unsigned int it = blockIdx.x * blockDim.x + threadIdx.x;
    myTimeLine[it] = t * it / (numT-1);;
}

// Same kernel with different values and grid size for initializing both myX and myY
__global__ void initMyXY(REAL * myXY, const REAL dxy, const REAL myXYPlus) {
    const unsigned int ixy = blockIdx.x * blockDim.x + threadIdx.x;
    myXY[ixy] = ixy * dxy + myXYPlus;
}

__global__ void initOperatorKernel( const unsigned int n, // numX or numY
                                    REAL *             x, 
                                    REAL *             Dxx
) {
    const unsigned int ixy = blockIdx.x * blockDim.x + threadIdx.x;
    
    // lower boundary
    if (ixy == 0) {
        Dxx[I2(0,0,n,4)] =  0.0;
        Dxx[I2(0,1,n,4)] =  0.0;
        Dxx[I2(0,2,n,4)] =  0.0;
        Dxx[I2(0,3,n,4)] =  0.0;
    }
    
    //  standard case
    else if (ixy < n - 1) {
        REAL dxl      = x[ixy]   - x[ixy - 1];
        REAL dxu      = x[ixy + 1] - x[ixy];

        Dxx[I2(ixy,0,n,4)] =  2.0/dxl/(dxl+dxu);
        Dxx[I2(ixy,1,n,4)] = -2.0*(1.0/dxl + 1.0/dxu)/(dxl+dxu);
        Dxx[I2(ixy,2,n,4)] =  2.0/dxu/(dxl+dxu);
        Dxx[I2(ixy,3,n,4)] =  0.0; 
    }

    //  upper boundary
    else if (ixy < n) {

        Dxx[I2((n - 1),0,n,4)] = 0.0;
        Dxx[I2((n - 1),1,n,4)] = 0.0;
        Dxx[I2((n - 1),2,n,4)] = 0.0;
        Dxx[I2((n - 1),3,n,4)] = 0.0;
    }

}

__global__ void setPayoffKernel(REAL* myResult, REAL* myX, const unsigned outer, const unsigned numX, const unsigned numY) {
    const unsigned int gid = blockIdx.x*blockDim.x + threadIdx.x;
    const unsigned int io = gid / (numX * numY);
    const unsigned int ix = (gid % (numX * numY)) / numY;
    const unsigned int iy = (gid % (numX * numY)) % numY;
    if (io < outer && ix < numX && iy < numY)
        myResult[gid] = max(myX[ix]-0.001*io, (REAL)0.0);
}

// NumT look Kernel

__global__ void updateParamsKernel(REAL * myVarX, REAL * myVarY, REAL * myX, REAL * myY, REAL nunuTime,
                                   const unsigned it, const unsigned numX, const unsigned numY,
                                   const REAL alpha, const REAL beta) {
    const unsigned int gid = blockIdx.x*blockDim.x + threadIdx.x;
    const unsigned int ix = gid / numY;
    const unsigned int iy = gid % numY;
    if (ix < numX && iy < numY) {
        myVarX[gid] = exp(2.0*(  beta*log(myX[ix])   
                                               + myY[iy]             
                                               - nunuTime )
                                         );
        myVarY[gid] = exp(2.0*(  alpha*log(myX[ix])   
                                               + myY[iy]             
                                               - nunuTime )
                                         ); 
    }
}

// Rollback Kernels

__device__ inline void tridagKernel(
    REAL *  a,   // size [n]
    REAL *  b,   // size [n]
    REAL *  c,   // size [n]
    REAL *  r,   // size [n]
    const unsigned int    io,
    const unsigned int    ic,
    const unsigned int    n, // rows
    const unsigned int    m, // cols
          REAL *          u,   // size [n]
          REAL *          uu   // size [n] temporary
) {
    int    i;
    REAL   beta;

    u[I2(0,ic,n,m)]  = r[I2(0,ic,n,m)];
    uu[I2(0,ic,n,m)] = b[I2(0,ic,n,m)];

    for(i=1; i<n; i++) {
        beta  = a[I2(i,ic,n,m)] / uu[I2((i-1),ic,n,m)]; // I2(i,ic,n,m)

        uu[I2(i,ic,n,m)] = b[I2(i,ic,n,m)] - beta*c[I2((i-1),ic,n,m)];
        u[I2(i,ic,n,m)]  = r[I2(i,ic,n,m)] - beta*u[I2((i-1),ic,n,m)];
    }

#if 1
    // X) this is a backward recurrence
    u[I2((n-1),ic,n,m)] = u[I2((n-1),ic,n,m)] / uu[I2((n-1),ic,n,m)];
    for(i=n-2; i>=0; i--) {
        u[I2(i,ic,n,m)] = (u[I2(i,ic,n,m)] - c[I2(i,ic,n,m)]*u[I2((i+1),ic,n,m)]) / uu[I2(i,ic,n,m)];
    }
#else
    // Hint: X) can be written smth like (once you make a non-constant)
    for(i=0; i<n; i++) a[i] =  u[n-1-i];
    a[0] = a[0] / uu[n-1];
    for(i=1; i<n; i++) a[i] = (a[i] - c[n-1-i]*a[i-1]) / uu[n-1-i];
    for(i=0; i<n; i++) u[i] = a[n-1-i];
#endif
}

// [outer][rows][cols]
__global__ void triCallKernel(REAL * a, REAL * b, REAL * c, REAL * r, REAL * u, REAL * uu,
                                  REAL dtInv, const unsigned outer, const unsigned rows, const unsigned cols) {
    const unsigned int gid = blockIdx.x*blockDim.x + threadIdx.x;
    const unsigned int io = gid / cols;
    const unsigned int ic = gid % cols;
    if (io < outer && ic < cols) {
        const unsigned offset = io * rows * cols;
        tridagKernel(a + offset, b + offset, c + offset, r + offset, io, ic, rows, cols, u + offset, uu + offset);
    }
}

__global__ void rollBackExplicitX(REAL * myResult, REAL * myVarX, REAL * myDxx, REAL * u, 
                                 REAL dtInv, const unsigned outer, const unsigned numX, const unsigned numY) {
    const unsigned int gid = blockIdx.x*blockDim.x + threadIdx.x;
    const unsigned int io = gid / (numX * numY);
    const unsigned int i2 = gid % (numX * numY);
    const unsigned int iy = i2 / numX;
    const unsigned int ix = i2 % numX;
    if (io < outer && ix < numX && iy < numY) {
        u[gid] = (dtInv + 0.5*( 0.5*myVarX[i2]*myDxx[I2(ix,1,numX,4)] ) )
                         * myResult[gid];
        if(ix > 0) { 
          u[gid] += 0.5*( 0.5*myVarX[i2]*myDxx[I2(ix,0,numX,4)] ) 
                        * myResult[gid - 1]; //I3(io,(ix - 1),iy,outer,numX,numY);
        }
        if(ix < numX-1) {
          u[gid] += 0.5*( 0.5*myVarX[i2]*myDxx[I2(ix,2,numX,4)] )
                        * myResult[gid + 1];
        }
    }
}


__global__ void rollBackExplicitY(REAL * myResult, REAL * myVarY, REAL * myDyy, REAL * u, REAL * v,
                                 REAL dtInv, const unsigned outer, const unsigned numX, const unsigned numY) {
    const unsigned int gid = blockIdx.x*blockDim.x + threadIdx.x;
    const unsigned int io = gid / (numX * numY);
    const unsigned int i2 = gid % (numX * numY);
    const unsigned int ix = i2 / numY;
    const unsigned int iy = i2 % numY;
    if (io < outer && ix < numX && iy < numY) {
        v[gid]  = ( 0.5*myVarY[i2]*myDyy[I2(iy,1,numY,4)] )
                        *  myResult[gid];

        if(iy > 0) {
          v[gid]  +=  ( 0.5*myVarY[i2]*myDyy[I2(iy,0,numY,4)] )
                        *  myResult[gid - 1];
        }
        if(iy < numY-1) {
          v[gid]  +=  ( 0.5*myVarY[i2]*myDyy[I2(iy,2,numY,4)] )
                        *  myResult[gid + 1];
        }
        u[gid] += v[gid] ; 
    }
}


__global__ void rollBackImplicitX(REAL * myVarX, REAL * myDxx, 
                                  REAL * a, REAL * b, REAL * c, 
                                  REAL dtInv, const unsigned outer, const unsigned numX, const unsigned numY) {
    const unsigned int gid = blockIdx.x*blockDim.x + threadIdx.x;
    const unsigned int io = gid / (numX * numY);
    const unsigned int i2 = gid % (numX * numY);
    const unsigned int ix = i2 / numY;
    const unsigned int iy = i2 % numY;
    if (io < outer && ix < numX && iy < numY) {// here a, b,c should have size [numX]
        a[gid] =       - 0.5*(0.5*myVarX[i2]*myDxx[I2(ix,0,numX,4)]);
        b[gid] = dtInv - 0.5*(0.5*myVarX[i2]*myDxx[I2(ix,1,numX,4)]);
        c[gid] =       - 0.5*(0.5*myVarX[i2]*myDxx[I2(ix,2,numX,4)]);
    }
}

__global__ void rollBackImplicitY(REAL * myVarY, REAL * myDyy, REAL * a, REAL * b, REAL * c, 
                                  REAL * u, REAL * v, REAL * y,
                                  REAL dtInv, const unsigned outer, const unsigned numX, const unsigned numY) {
    const unsigned int gid = blockIdx.x*blockDim.x + threadIdx.x;
    const unsigned int io = gid / (numX * numY);
    const unsigned int i2 = gid % (numX * numY);
    const unsigned int iy = i2 / numX;
    const unsigned int ix = i2 % numX;
    if (io < outer && ix < numX && iy < numY) { // here a, b, c should have size [numY]
           a[gid]  =       - 0.5*(0.5*myVarY[i2]*myDyy[I2(iy,0,numY,4)]);
           b[gid]  = dtInv - 0.5*(0.5*myVarY[i2]*myDyy[I2(iy,1,numY,4)]);
           c[gid]  =       - 0.5*(0.5*myVarY[i2]*myDyy[I2(iy,2,numY,4)]);
       
           y[gid] = dtInv * u[gid] - 0.5 * v[gid] ;
    }
}


// Ending Kernel

__global__ void setResultKernel(REAL * res, REAL * myResult, const unsigned myXindex, const unsigned myYindex,
                                const unsigned outer, const unsigned numX, const unsigned numY) {
    const unsigned int io = blockIdx.x * blockDim.x + threadIdx.x;
    if (io < outer)
        res[io] = myResult[I3(io, myXindex, myYindex, outer, numX, numY)];
}


// Helper Kernels

__global__ void sgmMatTranspose( REAL * A, REAL * trA, const unsigned int rowsA, const unsigned int colsA ) {
    __shared__ REAL tile[T2][T2+1];
    int gidz = blockIdx.z;
    A += gidz * rowsA * colsA; 
    trA += gidz * rowsA * colsA;

    // follows code for matrix transp in x & y
    int tidx = threadIdx.x;
    int tidy = threadIdx.y;
    int i = blockIdx.y * T2 + tidy;
    int j = blockIdx.x * T2 + tidx;
    if( j < colsA && i < rowsA )
        tile[tidy][tidx] = A[i * colsA + j];
    __syncthreads();
    i = blockIdx.y * T2 + tidx;
    j = blockIdx.x * T2 + tidy;
    if( j < colsA && i < rowsA )
        trA[j * rowsA + i] = tile[tidx][tidy];
}







#endif //KERS