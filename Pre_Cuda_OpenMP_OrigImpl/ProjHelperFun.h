#ifndef PROJ_HELPER_FUNS
#define PROJ_HELPER_FUNS

#include <vector>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include "Constants.h"

using namespace std;


struct PrivGlobs {

    //	grid
    vector<vector<REAL> >         myX;        // [outer][numX]
    vector<vector<REAL> >         myY;        // [outer][numY]
    vector<REAL>        myTimeline; // [numT]
    unsigned            myXindex;  // 
    unsigned            myYindex; // 

    //	variable
    vector<vector<vector<REAL> > > myResult; // [outer][numX][numY]

    //	coeffs
    vector<vector<vector<REAL> > >   myVarX; // [outer][numX][numY]
    vector<vector<vector<REAL> > >   myVarY; // [outer][numX][numY]

    //	operators
    vector<vector<vector<REAL> > >   myDxx;  // [outer][numX][4]
    vector<vector<vector<REAL> > >   myDyy;  // [outer][numY][4]

    PrivGlobs( ) {
        printf("Invalid Contructor: need to provide the array sizes! EXITING...!\n");
        exit(0);
    }

    PrivGlobs(  const unsigned int& outer,
                const unsigned int& numX,
                const unsigned int& numY,
                const unsigned int& numT ) {

        this->myTimeline.resize(numT);

        this->myX.resize(outer);
        this->myDxx.resize(outer);
        this->myY.resize(outer);
        this->myDyy.resize(outer);
        this->myVarX.resize(outer);
        this->myVarY.resize(outer);
        this->myResult.resize(outer);

        for (int io=0; io<outer; ++io) {
            this->  myX[io].resize(numX);
            this->myDxx[io].resize(numX);
            for(int k=0; k<numX; k++) {
                this->myDxx[io][k].resize(4);
            }

            this->  myY[io].resize(numY);
            this->myDyy[io].resize(numY);
            for(int k=0; k<numY; k++) {
                this->myDyy[io][k].resize(4);
            }

            this->  myVarX[io].resize(numX);
            this->  myVarY[io].resize(numX);
            this->myResult[io].resize(numX);
            for(unsigned i=0;i<numX;++i) {
                this->  myVarX[io][i].resize(numY);
                this->  myVarY[io][i].resize(numY);
                this->myResult[io][i].resize(numY);
            }
        }
    }
} __attribute__ ((aligned (128)));


void initGrid(  const REAL s0, const REAL alpha, const REAL nu,const REAL t, const unsigned int& outer,
                const unsigned numX, const unsigned numY, const unsigned numT, PrivGlobs& globs   
            );

void initOperator(  const vector<REAL>& x, 
                    vector<vector<REAL> >& Dxx
                 );

void updateParams(const unsigned it, const unsigned int& outer, const REAL alpha, const REAL beta, const REAL nu, PrivGlobs& globs);

void setPayoff(const REAL strike, PrivGlobs& globs );

void tridag(
    const vector<REAL>&   a,   // size [n]
    const vector<REAL>&   b,   // size [n]
    const vector<REAL>&   c,   // size [n]
    const vector<REAL>&   r,   // size [n]
    const int             n,
          vector<REAL>&   u,   // size [n]
          vector<REAL>&   uu   // size [n] temporary
);

void rollback(const unsigned        it, 
              const unsigned int&   outer, 
              const unsigned int&   numX,
              const unsigned int&   numY,
              PrivGlobs&            globs )
;

REAL   value(   PrivGlobs    globs,
                const REAL s0,
                const REAL strike, 
                const REAL t, 
                const REAL alpha, 
                const REAL nu, 
                const REAL beta,
                const unsigned int numX,
                const unsigned int numY,
                const unsigned int numT
            );

void run_OrigCPU(  
                const unsigned int&   outer,
                const unsigned int&   numX,
                const unsigned int&   numY,
                const unsigned int&   numT,
                const REAL&           s0,
                const REAL&           t, 
                const REAL&           alpha, 
                const REAL&           nu, 
                const REAL&           beta,
                      REAL*           res   // [outer] RESULT
            );

#endif // PROJ_HELPER_FUNS
