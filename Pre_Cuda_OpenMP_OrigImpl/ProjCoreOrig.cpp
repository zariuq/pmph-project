#include "ProjHelperFun.h"
#include "Constants.h"
#include "TridagPar.h"
#include <omp.h> // <-- Hmm, I needed it here too.

void updateParams(const unsigned it, const unsigned int& outer, const REAL alpha, 
                  const REAL beta, const REAL nu, PrivGlobs& globs)
{
    #pragma omp parallel for default(shared) schedule(static)
    for( unsigned io = 0; io < outer; ++io ) {
        for(unsigned ix=0;ix<globs.myX[io].size();++ix)
            for(unsigned iy=0;iy<globs.myY[io].size();++iy) {
                globs.myVarX[io][ix][iy] = exp(2.0*(  beta*log(globs.myX[io][ix])   
                                              + globs.myY[io][iy]             
                                              - 0.5*nu*nu*globs.myTimeline[it] )
                                        );
                globs.myVarY[io][ix][iy] = exp(2.0*(  alpha*log(globs.myX[io][ix])   
                                              + globs.myY[io][iy]             
                                              - 0.5*nu*nu*globs.myTimeline[it] )
                                        ); // nu*nu
            }
        }
}

void setPayoff(const unsigned io, PrivGlobs& globs )
{
    const REAL strike = 0.001*io;
	for(unsigned ix=0;ix<globs.myX[io].size();++ix)
	{
		REAL payoff = max(globs.myX[io][ix]-strike, (REAL)0.0);
		for(unsigned iy=0;iy<globs.myY[io].size();++iy)
			globs.myResult[io][ix][iy] = payoff;
	}
}

inline void tridag(
    const vector<REAL>&   a,   // size [n]
    const vector<REAL>&   b,   // size [n]
    const vector<REAL>&   c,   // size [n]
    const vector<REAL>&   r,   // size [n]
    const int             n,
          vector<REAL>&   u,   // size [n]
          vector<REAL>&   uu   // size [n] temporary
) {
    int    i, offset;
    REAL   beta;

    u[0]  = r[0];
    uu[0] = b[0];

    for(i=1; i<n; i++) {
        beta  = a[i] / uu[i-1];

        uu[i] = b[i] - beta*c[i-1];
        u[i]  = r[i] - beta*u[i-1];
    }

#if 1
    // X) this is a backward recurrence
    u[n-1] = u[n-1] / uu[n-1];
    for(i=n-2; i>=0; i--) {
        u[i] = (u[i] - c[i]*u[i+1]) / uu[i];
    }
#else
    // Hint: X) can be written smth like (once you make a non-constant)
    for(i=0; i<n; i++) a[i] =  u[n-1-i];
    a[0] = a[0] / uu[n-1];
    for(i=1; i<n; i++) a[i] = (a[i] - c[n-1-i]*a[i-1]) / uu[n-1-i];
    for(i=0; i<n; i++) u[i] = a[n-1-i];
#endif
}


void
rollback(const unsigned        it, 
         const unsigned int&   outer, 
         const unsigned int&   numX,
         const unsigned int&   numY,
         PrivGlobs&            globs ) {

    unsigned numZ = max(numX,numY);
    REAL dtInv = 1.0/(globs.myTimeline[it+1]-globs.myTimeline[it]);

    vector<vector<vector<REAL> > > u(outer, vector<vector<REAL> >(numY, vector<REAL>(numX)));   // [outer][numY][numX] io * numY * numX + iy * numX + ix?
    vector<vector<vector<REAL> > > v(outer, vector<vector<REAL> >(numX, vector<REAL>(numY)));   // [outer][numX][numY] 
    vector<vector<REAL> > a(outer, vector<REAL>(numZ)),
                          b(outer, vector<REAL>(numZ)),
                          c(outer, vector<REAL>(numZ)),
                          y(outer, vector<REAL>(numZ)); // [outer][max(numX,numY)]
    vector<vector<REAL> > yy(outer, vector<REAL>(numZ));  // temporary used in tridag  // [outer][max(numX,numY)]

    #pragma omp parallel for default(shared) schedule(static)
    for( unsigned io = 0; io < outer; ++io ) {

        unsigned ix, iy;

        //	explicit x
        for(ix=0;ix<numX;++ix) {
            for(iy=0;iy<numY;++iy) {
                u[io][iy][ix] = dtInv*globs.myResult[io][ix][iy];

                if(ix > 0) { 
                  u[io][iy][ix] += 0.5*( 0.5*globs.myVarX[io][ix][iy]*globs.myDxx[io][ix][0] ) 
                                * globs.myResult[io][ix - 1][iy];
                }
                u[io][iy][ix]   +=  0.5*( 0.5*globs.myVarX[io][ix][iy]*globs.myDxx[io][ix][1] )
                                * globs.myResult[io][ix][iy];
                if(ix < numX-1) {
                  u[io][iy][ix] += 0.5*( 0.5*globs.myVarX[io][ix][iy]*globs.myDxx[io][ix][2] )
                                * globs.myResult[io][ix + 1][iy];
                }
            }
        }
    }

    #pragma omp parallel for default(shared) schedule(static)
    for( unsigned io = 0; io < outer; ++io ) {

        unsigned ix, iy;

        //	explicit y
        for(iy=0;iy<numY;++iy)
        {
            for(ix=0;ix<numX;++ix) {
                v[io][ix][iy] = 0.0;

                if(iy > 0) {
                  v[io][ix][iy] +=  ( 0.5*globs.myVarY[io][ix][iy]*globs.myDyy[io][iy][0] )
                                *  globs.myResult[io][ix][iy - 1];
                }
                v[io][ix][iy]   +=   ( 0.5*globs.myVarY[io][ix][iy]*globs.myDyy[io][iy][1] )
                                *  globs.myResult[io][ix][iy];
                if(iy < numY-1) {
                  v[io][ix][iy] +=  ( 0.5*globs.myVarY[io][ix][iy]*globs.myDyy[io][iy][2] )
                                *  globs.myResult[io][ix][iy + 1];
                }
                u[io][iy][ix] += v[io][ix][iy]; 
            }
        }
    }

    #pragma omp parallel for default(shared) schedule(static)
    for( unsigned io = 0; io < outer; ++io ) {

        unsigned ix, iy;

        //	implicit x
        for(iy=0;iy<numY;++iy) {
            for(ix=0;ix<numX;++ix) {  // here a, b,c should have size [numX]
                a[io][ix] =		  - 0.5*(0.5*globs.myVarX[io][ix][iy]*globs.myDxx[io][ix][0]);
                b[io][ix] = dtInv - 0.5*(0.5*globs.myVarX[io][ix][iy]*globs.myDxx[io][ix][1]);
                c[io][ix] =		  - 0.5*(0.5*globs.myVarX[io][ix][iy]*globs.myDxx[io][ix][2]);
            }
            // here yy should have size [numX]
            tridagPar(a[io],b[io],c[io],u[io][iy],numX,u[io][iy],yy[io]);
        }
    }

    #pragma omp parallel for default(shared) schedule(static)
    for( unsigned io = 0; io < outer; ++io ) {

        unsigned ix, iy;

        //	implicit y
        for(ix=0;ix<numX;++ix) { 
            for(iy=0;iy<numY;++iy) {  // here a, b, c should have size [numY]
                a[io][iy] =		  - 0.5*(0.5*globs.myVarY[io][ix][iy]*globs.myDyy[io][iy][0]);
                b[io][iy] = dtInv - 0.5*(0.5*globs.myVarY[io][ix][iy]*globs.myDyy[io][iy][1]);
                c[io][iy] =		  - 0.5*(0.5*globs.myVarY[io][ix][iy]*globs.myDyy[io][iy][2]);
            }

            for(iy=0;iy<numY;++iy)
                y[io][iy] = dtInv * u[io][iy][ix] - 0.5 * v[io][ix][iy];

            // here yy should have size [numY]
            tridagPar(a[io],b[io],c[io],y[io],numY,globs.myResult[io][ix],yy[io]);
        }
    }
}

void   run_OrigCPU(  
                const unsigned int&   outer,
                const unsigned int&   numX,
                const unsigned int&   numY,
                const unsigned int&   numT,
                const REAL&           s0,
                const REAL&           t, 
                const REAL&           alpha, 
                const REAL&           nu, 
                const REAL&           beta,
                      REAL*           res   // [outer] RESULT
) {

    //vector<REAL> strike(outer); // strike[outer] --> hard-code for now
    PrivGlobs    globs(outer, numX, numY, numT); // Array-expanded
    initGrid(s0,alpha,nu,t, outer, numX, numY, numT, globs);
    // distribute outer-loop over
    // init loop
    #pragma omp parallel for default(shared) schedule(static)
    for (unsigned io = 0; io < outer; ++io) {
        initOperator(globs.myX[io],globs.myDxx[io]);
        initOperator(globs.myY[io],globs.myDyy[io]);
        setPayoff(io, globs);
    }
    for(int it = globs.myTimeline.size()-2; it>=0; --it) {
        // Should be a straightforward kernel, passing in the data
        updateParams(it, outer, alpha, beta, nu, globs);
        // Hard part
        rollback(it, outer, numX, numY, globs);
    }
    // Safe because each [io] array is modified independently
    #pragma omp parallel for default(shared) schedule(static)
    for (unsigned io = 0; io < outer; ++io) {
        res[io] = globs.myResult[io][globs.myXindex][globs.myYindex];
    }


}

//#endif // PROJ_CORE_ORIG
