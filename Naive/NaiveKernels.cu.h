#ifndef NAIVE_KERS
#define NAIVE_KERS

#include <cuda_runtime.h>
#include "Constants.h"

// Init Kernels

__global__ void initMyTimeLine(REAL * myTimeLine, const REAL t, const unsigned numT) {
    const unsigned int it = blockIdx.x * blockDim.x + threadIdx.x;
    myTimeLine[it] = t * it / (numT-1);;
}

// Same kernel with different values and grid size for initializing both myX and myY
__global__ void initMyXY(REAL * myXY, const REAL dxy, const REAL myXYPlus) {
    const unsigned int ixy = blockIdx.x * blockDim.x + threadIdx.x;
    myXY[ixy] = ixy * dxy + myXYPlus;
}

__global__ void initOperatorKernel( const unsigned int n, // numX or numY
                                    REAL *             x, 
                                    REAL *             Dxx
) {
    const unsigned int ixy = blockIdx.x * blockDim.x + threadIdx.x;
    
    // lower boundary
    if (ixy == 0) {
        Dxx[I2(0,0,n,4)] =  0.0;
        Dxx[I2(0,1,n,4)] =  0.0;
        Dxx[I2(0,2,n,4)] =  0.0;
        Dxx[I2(0,3,n,4)] =  0.0;
    }
    
    //  standard case
    else if (ixy < n - 1) {
        REAL dxl      = x[ixy]   - x[ixy - 1];
        REAL dxu      = x[ixy + 1] - x[ixy];

        Dxx[I2(ixy,0,n,4)] =  2.0/dxl/(dxl+dxu);
        Dxx[I2(ixy,1,n,4)] = -2.0*(1.0/dxl + 1.0/dxu)/(dxl+dxu);
        Dxx[I2(ixy,2,n,4)] =  2.0/dxu/(dxl+dxu);
        Dxx[I2(ixy,3,n,4)] =  0.0; 
    }

    //  upper boundary
    else if (ixy < n) {

        Dxx[I2((n - 1),0,n,4)] = 0.0;
        Dxx[I2((n - 1),1,n,4)] = 0.0;
        Dxx[I2((n - 1),2,n,4)] = 0.0;
        Dxx[I2((n - 1),3,n,4)] = 0.0;
    }

}

__global__ void setPayoffKernel(REAL* myResult, REAL* myX, const unsigned outer, const unsigned numX, const unsigned numY) {
    const unsigned int gid = blockIdx.x*blockDim.x + threadIdx.x;
    const unsigned int io = gid / (numX * numY);
    const unsigned int ix = (gid % (numX * numY)) / numY;
    const unsigned int iy = (gid % (numX * numY)) % numY;
    if (io < outer && ix < numX < iy < numY)
        myResult[gid] = max(myX[ix]-0.001*io, (REAL)0.0);
}

// NumT look Kernel

__global__ void updateParamsKernel(REAL * myVarX, REAL * myVarY, REAL * myX, REAL * myY, REAL nunuTime,
                                   const unsigned it, const unsigned numX, const unsigned numY,
                                   const REAL alpha, const REAL beta) {
    const unsigned int gid = blockIdx.x*blockDim.x + threadIdx.x;
    const unsigned int ix = gid / numY;
    const unsigned int iy = gid % numY;
    if (ix < numX && iy < numY) {
        myVarX[I2(ix,iy,numX,numY)] = exp(2.0*(  beta*log(myX[ix])   
                                               + myY[iy]             
                                               - nunuTime )
                                         );
        myVarY[I2(ix,iy,numX,numY)] = exp(2.0*(  alpha*log(myX[ix])   
                                               + myY[iy]             
                                               - nunuTime )
                                         ); 
    }
}

// Rollback Kernels

__device__ inline void tridagKernel(
    REAL *  a,   // size [n]
    REAL *  b,   // size [n]
    REAL *  c,   // size [n]
    REAL *  r,   // size [n]
    const int             n,
          REAL *          u,   // size [n]
          REAL *          uu   // size [n] temporary
) {
    int    i;
    REAL   beta;

    u[0]  = r[0];
    uu[0] = b[0];

    for(i=1; i<n; i++) {
        beta  = a[i] / uu[i-1];

        uu[i] = b[i] - beta*c[i-1];
        u[i]  = r[i] - beta*u[i-1];
    }

#if 1
    // X) this is a backward recurrence
    u[n-1] = u[n-1] / uu[n-1];
    for(i=n-2; i>=0; i--) {
        u[i] = (u[i] - c[i]*u[i+1]) / uu[i];
    }
#else
    // Hint: X) can be written smth like (once you make a non-constant)
    for(i=0; i<n; i++) a[i] =  u[n-1-i];
    a[0] = a[0] / uu[n-1];
    for(i=1; i<n; i++) a[i] = (a[i] - c[n-1-i]*a[i-1]) / uu[n-1-i];
    for(i=0; i<n; i++) u[i] = a[n-1-i];
#endif
}

__global__ void rollBackExplicitX(REAL * myResult, REAL * myVarX, REAL * myDxx, REAL * u, 
                                 REAL dtInv, const unsigned outer, const unsigned numX, const unsigned numY) {
    const unsigned int gid = blockIdx.x*blockDim.x + threadIdx.x;
    const unsigned int io = gid / numX;
    const unsigned int ix = gid % numX;
    if (io < outer && ix < numX) {
        for(unsigned iy=0;iy<numY;++iy) {
            u[I3(io,iy,ix,outer,numY,numX)] = dtInv*myResult[I3(io,ix,iy,outer,numX,numY)];

            if(ix > 0) { 
              u[I3(io,iy,ix,outer,numY,numX)] += 0.5*( 0.5*myVarX[I2(ix,iy,numX,numY)]*myDxx[I2(ix,0,numX,4)] ) 
                            * myResult[I3(io,(ix - 1),iy,outer,numX,numY)]; //I3(io,ix - 1,iy,outer,numX,numY)];
            }
            u[I3(io,iy,ix,outer,numY,numX)]   +=  0.5*( 0.5*myVarX[I2(ix,iy,numX,numY)]*myDxx[I2(ix,1,numX,4)] )
                            * myResult[I3(io,ix,iy,outer,numX,numY)];
            if(ix < numX-1) {
              u[I3(io,iy,ix,outer,numY,numX)] += 0.5*( 0.5*myVarX[I2(ix,iy,numX,numY)]*myDxx[I2(ix,2,numX,4)] )
                            * myResult[I3(io,(ix + 1),iy,outer,numX,numY)];
            }
        }
    }
}

__global__ void rollBackExplicitY(REAL * myResult, REAL * myVarY, REAL * myDyy, REAL * u, REAL * v,
                                 REAL dtInv, const unsigned outer, const unsigned numX, const unsigned numY) {
    const unsigned int gid = blockIdx.x*blockDim.x + threadIdx.x;
    const unsigned int io = gid / numY;
    const unsigned int iy = gid % numY;
    if (io < outer && iy < numY) {
        for(unsigned ix=0;ix<numX;++ix) {
            v[I3(io,ix,iy,outer,numX,numY)]  = 0.0;

            if(iy > 0) {
              v[I3(io,ix,iy,outer,numX,numY)]  +=  ( 0.5*myVarY[I2(ix,iy,numX,numY)]*myDyy[I2(iy,0,numY,4)] )
                            *  myResult[I3(io,ix,(iy - 1),outer,numX,numY)];
            }
            v[I3(io,ix,iy,outer,numX,numY)]    +=   ( 0.5*myVarY[I2(ix,iy,numX,numY)]*myDyy[I2(iy,1,numY,4)] )
                            *  myResult[I3(io,ix,iy,outer,numX,numY)];
            if(iy < numY-1) {
              v[I3(io,ix,iy,outer,numX,numY)]  +=  ( 0.5*myVarY[I2(ix,iy,numX,numY)]*myDyy[I2(iy,2,numY,4)] )
                            *  myResult[I3(io,ix,(iy + 1),outer,numX,numY)];
            }
            u[I3(io,iy,ix,outer,numY,numX)] += v[I3(io,ix,iy,outer,numX,numY)] ; 
        }
    }
}

__global__ void rollBackImplicitX(REAL * myVarX, REAL * myDxx, 
                                  REAL * a, REAL * b, REAL * c, REAL * u, REAL * yy,
                                  REAL dtInv, const unsigned outer, const unsigned numX, const unsigned numY) {
    const unsigned int gid = blockIdx.x*blockDim.x + threadIdx.x;
    const unsigned int io = gid / numY;
    const unsigned int iy = gid % numY;
    if (io < outer && iy < numY) {
        for(unsigned ix=0;ix<numX;++ix) {  // here a, b,c should have size [numX]
            a[I3(io,iy,ix,outer,numY,numX)] =       - 0.5*(0.5*myVarX[I2(ix,iy,numX,numY)]*myDxx[I2(ix,0,numX,4)]);
            b[I3(io,iy,ix,outer,numY,numX)] = dtInv - 0.5*(0.5*myVarX[I2(ix,iy,numX,numY)]*myDxx[I2(ix,1,numX,4)]);
            c[I3(io,iy,ix,outer,numY,numX)] =       - 0.5*(0.5*myVarX[I2(ix,iy,numX,numY)]*myDxx[I2(ix,2,numX,4)]);
        }
        const unsigned offset = io * numY * numX + iy * numX; // (i,j,k,M,N,U) (i * N * U + j* U + k)
        tridagKernel(a + offset, b + offset, c + offset, u + offset, numX, u + offset, yy + offset);
    }
}

__global__ void rollBackImplicitY(REAL * myResult, REAL * myVarY, REAL * myDyy, 
                                  REAL * a, REAL * b, REAL * c, REAL * u, REAL * v, REAL * y, REAL * yy,
                                  REAL dtInv, const unsigned outer, const unsigned numX, const unsigned numY) {
    const unsigned int gid = blockIdx.x*blockDim.x + threadIdx.x;
    const unsigned int io = gid / numX;
    const unsigned int ix = gid % numX;
    if (io < outer && ix < numX) {
        const unsigned offset = gid * numY; // (i,j,k,M,N,U) (i * N * U + j* U + k)
        for(unsigned iy=0;iy<numY;++iy) {  // here a, b, c should have size [numY]
               a[offset + iy]  =       - 0.5*(0.5*myVarY[I2(ix,iy,numX,numY)]*myDyy[I2(iy,0,numY,4)]);
               b[offset + iy]  = dtInv - 0.5*(0.5*myVarY[I2(ix,iy,numX,numY)]*myDyy[I2(iy,1,numY,4)]);
               c[offset + iy]  =       - 0.5*(0.5*myVarY[I2(ix,iy,numX,numY)]*myDyy[I2(iy,2,numY,4)]);
           
               y[offset + iy] = dtInv * u[I3(io,iy,ix,outer,numY,numX)] - 0.5 * v[offset + iy] ;
            }
        tridagKernel(a + offset, b + offset, c + offset, y + offset, numY, myResult + offset, yy + offset);
    }
}

// Ending Kernel

__global__ void setResultKernel(REAL * res, REAL * myResult, const unsigned myXindex, const unsigned myYindex,
                                const unsigned outer, const unsigned numX, const unsigned numY) {
    const unsigned int io = blockIdx.x * blockDim.x + threadIdx.x;
    if (io < outer)
        res[io] = myResult[I3(io, myXindex, myYindex, outer, numX, numY)];
}



#endif //NAIVE_KERS