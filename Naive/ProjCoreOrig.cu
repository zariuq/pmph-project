#include "ProjHelperFun.cu.h"
#include "Constants.h"
#include "TridagPar.cu.h"
#include "NaiveKernels.cu.h"

/**
 * Fills in:
 *   globs.myTimeline  of size [0..numT-1]
 *   globs.myX         of size [0..numX-1]
 *   globs.myY         of size [0..numY-1]
 * and also sets
 *   globs.myXindex and globs.myYindex (both scalars)
 */
void initGrid(  const REAL s0, const REAL alpha, const REAL nu, const REAL t,
                const unsigned numX, const unsigned numY, const unsigned numT, 
                PrivGlobs& cudas
) {
    int dimt = ceil( (float)numT / T1);
    int dimx = ceil( (float)numX / T1 );
    int dimy = ceil( (float)numY / T1 );
    dim3 gridt(dimt, 1, 1);
    dim3 gridx(dimx, 1, 1);
    dim3 gridy(dimy, 1, 1);
    dim3 block(T1, 1, 1);

    initMyTimeLine<<<gridt, block>>>(cudas.d_myTimeline, t, numT);


    const REAL stdX = 20.0*alpha*s0*sqrt(t);
    const REAL dx = stdX/numX;
    cudas.myXindex = static_cast<unsigned>(s0/dx) % numX; // it's just a constant
    const REAL myXPlus = s0 - cudas.myXindex * dx; // To have fewer parameters to send to GPU

    initMyXY<<<gridx, block>>>(cudas.myX, dx, myXPlus);

    const REAL stdY = 10.0*nu*sqrt(t);
    const REAL dy = stdY/numY;
    const REAL logAlpha = log(alpha);
    cudas.myYindex = static_cast<unsigned>(numY/2.0);
    const REAL myYPlus = log(alpha) - cudas.myYindex * dy;

    initMyXY<<<gridy, block>>>(cudas.myY, dy, myYPlus);
    
    cudaThreadSynchronize();
    cudaMemcpy(cudas.myTimeline, cudas.d_myTimeline, numT * sizeof(REAL), cudaMemcpyDeviceToHost);

}

void initOperators(const unsigned numX, const unsigned numY,
                   PrivGlobs& cudas
) {
    int dimx = ceil( (float)numX / T1 );
    int dimy = ceil( (float)numY / T1 );
    dim3 gridx(dimx, 1, 1);
    dim3 gridy(dimy, 1, 1);
    dim3 block(T1, 1, 1);

    initOperatorKernel<<<gridx, block>>>(numX, cudas.myX, cudas.myDxx);
    initOperatorKernel<<<gridy, block>>>(numY, cudas.myY, cudas.myDyy);
    cudaThreadSynchronize();
    }

void setPayoff(const unsigned int&   outer,
               const unsigned int&   numX,
               const unsigned int&   numY, 
               PrivGlobs& cudas) {
    int dim = ceil( (float)(outer * numX * numY)/ T1 );
    dim3 grid(dim, 1, 1);
    dim3 block(T1, 1, 1);
    setPayoffKernel<<<grid, block>>>(cudas.myResult, cudas.myX, outer, numX, numY);
    cudaThreadSynchronize();
}


void updateParams(const unsigned it, 
                  const unsigned int&   numX,
                  const unsigned int&   numY,
                  const REAL alpha, 
                  const REAL beta, 
                  const REAL nu, 
                  PrivGlobs& cudas)
{
    int dim = ceil( (float)(numX * numY)/ T1 );
    dim3 grid(dim, 1, 1);
    dim3 block(T1, 1, 1);
    REAL nunuTime = 0.5 * nu * nu * cudas.myTimeline[it];
    updateParamsKernel<<<grid, block>>>(cudas.myVarX, cudas.myVarY, cudas.myX, cudas.myY, nunuTime,
                                        it, numX, numY, alpha, beta);
    cudaThreadSynchronize();
}

inline void tridag(
    REAL *  a,   // size [n]
    REAL *  b,   // size [n]
    REAL *  c,   // size [n]
    REAL *  r,   // size [n]
    const int             n,
          REAL *          u,   // size [n]
          REAL *          uu   // size [n] temporary
) {
    int    i;
    REAL   beta;

    u[0]  = r[0];
    uu[0] = b[0];

    for(i=1; i<n; i++) {
        beta  = a[i] / uu[i-1];

        uu[i] = b[i] - beta*c[i-1];
        u[i]  = r[i] - beta*u[i-1];
    }

#if 1
    // X) this is a backward recurrence
    u[n-1] = u[n-1] / uu[n-1];
    for(i=n-2; i>=0; i--) {
        u[i] = (u[i] - c[i]*u[i+1]) / uu[i];
    }
#else
    // Hint: X) can be written smth like (once you make a non-constant)
    for(i=0; i<n; i++) a[i] =  u[n-1-i];
    a[0] = a[0] / uu[n-1];
    for(i=1; i<n; i++) a[i] = (a[i] - c[n-1-i]*a[i-1]) / uu[n-1-i];
    for(i=0; i<n; i++) u[i] = a[n-1-i];
#endif
}


void
rollback(const unsigned        it, 
         const unsigned int&   outer, 
         const unsigned int&   numX,
         const unsigned int&   numY,
         PrivGlobs&            cudas,
         RollArrs&             arrs ) {

    REAL dtInv = 1.0/(cudas.myTimeline[it+1]-cudas.myTimeline[it]);

    //const unsigned int size = outer * numX * numY;
    int dimox = ceil( (float)(outer * numX)/ T1 );
    int dimoy = ceil( (float)(outer * numY)/ T1 );
    dim3 gridox(dimox, 1, 1);
    dim3 gridoy(dimoy, 1, 1);
    dim3 block(T1, 1, 1);

    int dimo = ceil( (float)outer / T2 );
    int dimx = ceil( (float)numX / T2 );
    int dimy = ceil( (float)numY / T2 );
    dim3 gridoy2(dimo, dimy, 1);
    dim3 block2(T2, T2, 1);

    rollBackExplicitX<<<gridox, block>>>(cudas.myResult, cudas.myVarX, cudas.myDxx, arrs.u, 
                                          dtInv, outer, numX, numY);
    cudaThreadSynchronize();

    rollBackExplicitY<<<gridoy, block>>>(cudas.myResult, cudas.myVarY, cudas.myDyy, arrs.u, arrs.v, 
                                          dtInv, outer, numX, numY);
    cudaThreadSynchronize();

    rollBackImplicitX<<<gridoy, block>>>(cudas.myVarX, cudas.myDxx, arrs.a, arrs.b, arrs.c, arrs.u, arrs.yy,
                                          dtInv, outer, numX, numY);
    cudaThreadSynchronize();

    rollBackImplicitY<<<gridox, block>>>(cudas.myResult, cudas.myVarY, cudas.myDyy, arrs.a, arrs.b, arrs.c, 
                                          arrs.u, arrs.v, arrs.y, arrs.yy, dtInv, outer, numX, numY);
    cudaThreadSynchronize();
}

void setResult(REAL* res, const unsigned int outer, const unsigned int numX, const unsigned int numY,
               PrivGlobs& cudas) {
    int dimo = ceil( (float)outer / T1 );
    dim3 grid(dimo, 1, 1);
    dim3 block(T1, 1, 1);

    REAL * d_res;
    cudaMalloc((void**)&d_res, outer * sizeof(REAL));
    setResultKernel<<<grid, block>>>(d_res, cudas.myResult, cudas.myXindex, cudas.myYindex, outer, numX, numY);
    cudaThreadSynchronize();
    cudaMemcpy(res, d_res, outer * sizeof(REAL), cudaMemcpyDeviceToHost);
    cudaFree(d_res);
}

void   run_OrigCPU(  
                const unsigned int&   outer,
                const unsigned int&   numX,
                const unsigned int&   numY,
                const unsigned int&   numT,
                const REAL&           s0,
                const REAL&           t, 
                const REAL&           alpha, 
                const REAL&           nu, 
                const REAL&           beta,
                      REAL*           res   // [outer] RESULT
) {

    PrivGlobs    cudas(true, outer, numX, numY, numT); // Cuda allocated
    RollArrs     arrs(outer, numX, numY); // Cuda allocated arrays for rollback
    initGrid(s0, alpha, nu, t, numX, numY, numT, cudas);
    initOperators(numX, numY, cudas);
    setPayoff(outer, numX, numY, cudas);

    for(int it = numT-2; it>=0; --it) {
        // Should be a straightforward kernel, passing in the data
        updateParams(it, numX, numY, alpha, beta, nu, cudas);
        // Hard part
        rollback(it, outer, numX, numY, cudas, arrs);
    }
    // Safe because each [io] array is modified independently
    setResult(res, outer, numX, numY, cudas);

    cudas.free(true);

}

//#endif // PROJ_CORE_ORIG
