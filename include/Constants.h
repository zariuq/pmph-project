#ifndef CONSTANTS
#define CONSTANTS

#if (WITH_FLOATS==0)
    typedef double REAL;
#else
    typedef float  REAL;
#endif

#define T1 512
#define T2 32
#define T3 8
#define Ttri 256
#define I2(i,j,M,N) (i*N + j)
#define I3(i,j,k,M,N,U) (i * N * U + j* U + k)

#endif // CONSTANTS
